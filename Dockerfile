### Build the code
FROM golang:1.12-alpine
ENV CGO_ENABLED=0 GO111MODULE=on GOOS=linux GOARCH=amd64
RUN apk add --update git
WORKDIR /go/src/git.sr.ht/~hokiegeek/teadb
COPY . .
RUN go install -v -ldflags="-w -s" ./...

### Package it up
FROM alpine
VOLUME /conf
EXPOSE 80 443
ENV GOOGLE_APPLICATION_CREDENTIALS=/conf/hgnet-teadb.json
RUN apk add --no-cache --update ca-certificates
# RUN addgroup -S gouser && adduser -S -G gouser gouser
# USER gouser
COPY --from=0 /go/bin/teadbd /
ONBUILD ENTRYPOINT ["/teadbd"]
ENTRYPOINT ["/teadbd"]
