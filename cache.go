package teadb

import (
	"log"
)

// Cache is a cache struct
type Cache struct {
	allTeasValid bool
	teas         []Tea
	teaByID      map[int]Tea
}

// Invalidate invalidates everything cached
func (c *Cache) Invalidate() {
	c.allTeasValid = false
	c.teaByID = make(map[int]Tea)
	log.Println("Invalidated cache")
}

// AllTeas returns the teas
func (c *Cache) AllTeas() ([]Tea, bool) {
	return c.teas, c.allTeasValid
}

// AddAll caches teas array
func (c *Cache) AddAll(t []Tea) {
	c.allTeasValid, c.teas = true, t
}

// Tea returns a cached tea
func (c *Cache) Tea(id int) (t Tea, ok bool) {
	t, ok = c.teaByID[id]
	return
}

// Add returns a cached tea
func (c *Cache) Add(tea Tea) {
	c.teaByID[tea.ID] = tea
}

// NewCache creates a new Cache object
func NewCache() (*Cache, error) {
	return &Cache{teaByID: make(map[int]Tea)}, nil
}
