package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"git.sr.ht/~hokiegeek/teadb"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	portPtr := flag.Int("port", 80, "Specify the port to use")
	projectIDPtr := flag.String("project", "", "Need to know the project name")

	flag.Parse()

	fmt.Printf("Serving on port: %d\n", *portPtr)

	db, err := teadb.New(*projectIDPtr)
	if err != nil {
		log.Fatalf("could not start db client: %v", err)
	}

	cache, err := teadb.NewCache()
	if err != nil {
		log.Fatalf("could not initiate db cache: %v", err)
	}

	r := mux.NewRouter()

	r.HandleFunc("/teas",
		func(w http.ResponseWriter, r *http.Request) {
			getAllTeasHandler(w, r, db, cache)
		}).Methods("HEAD", "GET", "OPTIONS")

	r.HandleFunc("/tea/{id:[0-9]+}",
		func(w http.ResponseWriter, r *http.Request) {
			teaHandler(w, r, db, cache)
		}).Methods("HEAD", "GET", "POST", "PUT", "DELETE", "OPTIONS")

	r.HandleFunc("/tea/{teaid:[0-9]+}/entry",
		func(w http.ResponseWriter, r *http.Request) {
			entryHandler(w, r, db, cache)
		}).Methods("HEAD", "POST", "PUT", "OPTIONS")

	r.HandleFunc("/tea/{teaid:[0-9]+}/entry/{entryid:[0-9]*}",
		func(w http.ResponseWriter, r *http.Request) {
			entryHandler(w, r, db, cache)
		}).Methods("HEAD", "GET", "PUT", "DELETE", "OPTIONS")

	originsOk := handlers.AllowedOrigins([]string{"*"})
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "If-None-Match"})
	exposedOk := handlers.ExposedHeaders([]string{"Content-Type", "Content-Length", "Accept-Encoding", "Authorization", "Etag"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	http.ListenAndServe(fmt.Sprintf(":%d", *portPtr), handlers.CORS(originsOk, headersOk, methodsOk, exposedOk)(r))
}

func getAllTeasHandler(w http.ResponseWriter, r *http.Request, db *teadb.GcpClient, cache *teadb.Cache) {
	log.Printf("%s /teas [%s]\n", r.Method, r.RemoteAddr)

	if r.Method == http.MethodOptions {
		return
	}

	teas, valid := cache.AllTeas()
	if !valid {
		var err error
		teas, err = db.AllTeas()
		if err != nil {
			log.Printf("error: could not retrieve all teas: %v\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		cache.AddAll(teas)
		log.Println("Retrieved and cached")
	}
	postJSON(w, r, teas)
}

func teaHandler(w http.ResponseWriter, r *http.Request, db *teadb.GcpClient, cache *teadb.Cache) {
	vars := mux.Vars(r)

	log.Printf("%s /tea/%s [%s]\n", r.Method, vars["id"], r.RemoteAddr)

	if r.Method == http.MethodOptions {
		return
	}

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Printf("error: did not get tea ID: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	readTea := func() (tea teadb.Tea, err error) {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("error reading tea body: %v", err)
			http.Error(w, "can't read body", http.StatusBadRequest)
			return
		}

		// TODO: need some way to validate this

		if err = json.Unmarshal(body, &tea); err != nil {
			log.Printf("could not unmarshal: %v\n", err)
			http.Error(w, "can't read tea", http.StatusUnprocessableEntity)
		}

		return
	}

	switch r.Method {
	case http.MethodHead:
	case http.MethodGet:
		tea, valid := cache.Tea(id)
		if !valid {
			var err error
			tea, err = db.TeaByID(id)
			if err != nil {
				log.Printf("error: could not retrieve tea with id %d: %v\n", id, err)
				w.WriteHeader(http.StatusNotFound)
				return
			}
			cache.Add(tea)
		}
		postJSON(w, r, tea)
	case http.MethodPost:
		// Create new TEA
		if _, err := db.TeaByID(id); err == nil {
			w.WriteHeader(http.StatusConflict)
			return
		}
		if tea, err := readTea(); err == nil {
			if err = db.CreateTea(tea); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				cache.Invalidate()
				w.WriteHeader(http.StatusCreated)
			}
		}
	case http.MethodPut:
		// Update existing TEA
		if _, err := db.TeaByID(id); err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if tea, err := readTea(); err == nil {
			if err = db.UpdateTea(tea); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				cache.Invalidate()
				w.WriteHeader(http.StatusOK)
			}
		}
	case http.MethodDelete:
		// Delete existing TEA
		if _, err := db.TeaByID(id); err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err = db.DeleteTea(id); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			cache.Invalidate()
			w.WriteHeader(http.StatusOK)
		}
	}
}

func entryHandler(w http.ResponseWriter, r *http.Request, db *teadb.GcpClient, cache *teadb.Cache) {
	vars := mux.Vars(r)

	log.Printf("%s /tea/%s/entry/%s [%s]\n", r.Method, vars["teaid"], vars["entryid"], r.RemoteAddr)

	if r.Method == http.MethodOptions {
		return
	}

	teaid, err := strconv.Atoi(vars["teaid"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	tea, err := db.TeaByID(teaid)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var entryid int64
	if len(vars["entryid"]) > 0 && r.Method != http.MethodPost && r.Method != http.MethodPut {
		entryid, err = strconv.ParseInt(vars["entryid"], 10, 64)
		if err != nil {
			http.Error(w, "could not recognize entryid", http.StatusBadRequest)
			return
		}
	}

	readEntry := func() (entry teadb.TeaEntry, err error) {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("error reading entry body: %v", err)
			http.Error(w, "can't read body", http.StatusBadRequest)
			return
		}

		// TODO: need some way to validate this

		if err = json.Unmarshal(body, &entry); err != nil {
			log.Printf("could not unmarshal: %v\n", err)
			http.Error(w, "can't read entry", http.StatusUnprocessableEntity)
		}

		return
	}

	switch r.Method {
	case http.MethodHead:
	case http.MethodGet:
		for _, teaEntry := range tea.Entries {
			if entryid == teaEntry.Datetime.UnixNano() {
				postJSON(w, r, teaEntry)
				return
			}
		}
		w.WriteHeader(http.StatusNotFound)
	case http.MethodPost:
		if entry, err := readEntry(); err == nil {
			if err = db.CreateEntry(tea.ID, entry); err != nil {
				http.Error(w, "error creating new entry", http.StatusInternalServerError)
			} else {
				cache.Invalidate()
				w.WriteHeader(http.StatusCreated)
			}
		}
	case http.MethodPut:
		// Update existing tea entry
		if entry, err := readEntry(); err == nil {
			if err = db.UpdateEntry(tea.ID, entry); err != nil {
				http.Error(w, "error updating entry", http.StatusInternalServerError)
			} else {
				cache.Invalidate()
				w.WriteHeader(http.StatusOK)
			}
		}
	case http.MethodDelete:
		for i, teaEntry := range tea.Entries {
			if entryid == teaEntry.Datetime.Unix() {
				tea.Entries = append(tea.Entries[:i], tea.Entries[i+1:]...)
				break
			}
		}

		if err = db.UpdateTea(tea); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		} else {
			cache.Invalidate()
			w.WriteHeader(http.StatusOK)
		}
	}
}

func postJSON(w http.ResponseWriter, r *http.Request, payload interface{}) {
	JSON, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	requestedSum := r.Header.Get("If-None-Match")
	sum := checksum(JSON)
	if requestedSum == sum {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	w.Header().Set("Etag", sum)
	if r.Method != http.MethodGet {
		w.Header().Add("Content-Length", "0")
		w.WriteHeader(http.StatusOK)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err = json.NewEncoder(w).Encode(payload); err != nil {
		log.Printf("Error sending payload: %v", err)
		http.Error(w, "error sending payload", http.StatusInternalServerError)
	}
}

func checksum(body []byte) string {
	hash := md5.Sum(body)
	return hex.EncodeToString(hash[:])
}
